syntax = "proto3";

package gitaly;

import "shared.proto";

service DiffService {
  // Returns stream of CommitDiffResponse with patches chunked over messages
  rpc CommitDiff(CommitDiffRequest) returns (stream CommitDiffResponse) {};
  // Return a stream so we can divide the response in chunks of deltas
  rpc CommitDelta(CommitDeltaRequest) returns (stream CommitDeltaResponse) {};
}

message CommitDiffRequest {
  Repository repository = 1;
  string left_commit_id = 2;
  string right_commit_id = 3;
  bool ignore_whitespace_change = 4;
  repeated bytes paths = 5;
  bool collapse_diffs = 6;
  bool enforce_limits = 7;
  int32 max_files = 8;
  int32 max_lines = 9;
  int32 max_bytes = 10;
  int32 safe_max_files = 11;
  int32 safe_max_lines = 12;
  int32 safe_max_bytes = 13;
}

// A CommitDiffResponse corresponds to a single changed file in a commit.
message CommitDiffResponse {
  reserved 8;

  bytes from_path = 1;
  bytes to_path = 2;
  // Blob ID as returned via `git diff --full-index`
  string from_id = 3;
  string to_id = 4;
  int32 old_mode = 5;
  int32 new_mode = 6;
  bool binary = 7;
  bytes raw_patch_data = 9;
  bool end_of_patch = 10;
  // Indicates the diff file at which we overflow according to the limitations sent,
  // in which case only this attribute will be set.
  bool overflow_marker = 11;
  bool collapsed = 12;
}

message CommitDeltaRequest {
  Repository repository = 1;
  string left_commit_id = 2;
  string right_commit_id = 3;
  repeated bytes paths = 4;
}

message CommitDelta {
  bytes from_path = 1;
  bytes to_path = 2;
  // Blob ID as returned via `git diff --full-index`
  string from_id = 3;
  string to_id = 4;
  int32 old_mode = 5;
  int32 new_mode = 6;
}

message CommitDeltaResponse {
  repeated CommitDelta deltas = 1;
}
