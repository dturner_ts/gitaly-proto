# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: ref.proto

require 'google/protobuf'

require 'shared_pb'
require 'google/protobuf/timestamp_pb'
Google::Protobuf::DescriptorPool.generated_pool.build do
  add_message "gitaly.FindDefaultBranchNameRequest" do
    optional :repository, :message, 1, "gitaly.Repository"
  end
  add_message "gitaly.FindDefaultBranchNameResponse" do
    optional :name, :bytes, 1
  end
  add_message "gitaly.FindAllBranchNamesRequest" do
    optional :repository, :message, 1, "gitaly.Repository"
  end
  add_message "gitaly.FindAllBranchNamesResponse" do
    repeated :names, :bytes, 1
  end
  add_message "gitaly.FindAllTagNamesRequest" do
    optional :repository, :message, 1, "gitaly.Repository"
  end
  add_message "gitaly.FindAllTagNamesResponse" do
    repeated :names, :bytes, 1
  end
  add_message "gitaly.FindRefNameRequest" do
    optional :repository, :message, 1, "gitaly.Repository"
    optional :commit_id, :string, 2
    optional :prefix, :bytes, 3
  end
  add_message "gitaly.FindRefNameResponse" do
    optional :name, :bytes, 1
  end
  add_message "gitaly.FindLocalBranchesRequest" do
    optional :repository, :message, 1, "gitaly.Repository"
    optional :sort_by, :enum, 2, "gitaly.FindLocalBranchesRequest.SortBy"
  end
  add_enum "gitaly.FindLocalBranchesRequest.SortBy" do
    value :NAME, 0
    value :UPDATED_ASC, 1
    value :UPDATED_DESC, 2
  end
  add_message "gitaly.FindLocalBranchesResponse" do
    repeated :branches, :message, 1, "gitaly.FindLocalBranchResponse"
  end
  add_message "gitaly.FindLocalBranchResponse" do
    optional :name, :bytes, 1
    optional :commit_id, :string, 2
    optional :commit_subject, :bytes, 3
    optional :commit_author, :message, 4, "gitaly.FindLocalBranchCommitAuthor"
    optional :commit_committer, :message, 5, "gitaly.FindLocalBranchCommitAuthor"
  end
  add_message "gitaly.FindLocalBranchCommitAuthor" do
    optional :name, :bytes, 1
    optional :email, :bytes, 2
    optional :date, :message, 3, "google.protobuf.Timestamp"
  end
  add_message "gitaly.FindAllBranchesRequest" do
    optional :repository, :message, 1, "gitaly.Repository"
  end
  add_message "gitaly.FindAllBranchesResponse" do
    repeated :branches, :message, 1, "gitaly.FindAllBranchesResponse.Branch"
  end
  add_message "gitaly.FindAllBranchesResponse.Branch" do
    optional :name, :bytes, 1
    optional :target, :message, 2, "gitaly.GitCommit"
  end
  add_message "gitaly.FindAllTagsRequest" do
    optional :repository, :message, 1, "gitaly.Repository"
  end
  add_message "gitaly.FindAllTagsResponse" do
    repeated :tags, :message, 1, "gitaly.FindAllTagsResponse.Tag"
  end
  add_message "gitaly.FindAllTagsResponse.Tag" do
    optional :name, :bytes, 1
    optional :id, :string, 2
    optional :target_commit, :message, 3, "gitaly.GitCommit"
    optional :message, :bytes, 4
  end
end

module Gitaly
  FindDefaultBranchNameRequest = Google::Protobuf::DescriptorPool.generated_pool.lookup("gitaly.FindDefaultBranchNameRequest").msgclass
  FindDefaultBranchNameResponse = Google::Protobuf::DescriptorPool.generated_pool.lookup("gitaly.FindDefaultBranchNameResponse").msgclass
  FindAllBranchNamesRequest = Google::Protobuf::DescriptorPool.generated_pool.lookup("gitaly.FindAllBranchNamesRequest").msgclass
  FindAllBranchNamesResponse = Google::Protobuf::DescriptorPool.generated_pool.lookup("gitaly.FindAllBranchNamesResponse").msgclass
  FindAllTagNamesRequest = Google::Protobuf::DescriptorPool.generated_pool.lookup("gitaly.FindAllTagNamesRequest").msgclass
  FindAllTagNamesResponse = Google::Protobuf::DescriptorPool.generated_pool.lookup("gitaly.FindAllTagNamesResponse").msgclass
  FindRefNameRequest = Google::Protobuf::DescriptorPool.generated_pool.lookup("gitaly.FindRefNameRequest").msgclass
  FindRefNameResponse = Google::Protobuf::DescriptorPool.generated_pool.lookup("gitaly.FindRefNameResponse").msgclass
  FindLocalBranchesRequest = Google::Protobuf::DescriptorPool.generated_pool.lookup("gitaly.FindLocalBranchesRequest").msgclass
  FindLocalBranchesRequest::SortBy = Google::Protobuf::DescriptorPool.generated_pool.lookup("gitaly.FindLocalBranchesRequest.SortBy").enummodule
  FindLocalBranchesResponse = Google::Protobuf::DescriptorPool.generated_pool.lookup("gitaly.FindLocalBranchesResponse").msgclass
  FindLocalBranchResponse = Google::Protobuf::DescriptorPool.generated_pool.lookup("gitaly.FindLocalBranchResponse").msgclass
  FindLocalBranchCommitAuthor = Google::Protobuf::DescriptorPool.generated_pool.lookup("gitaly.FindLocalBranchCommitAuthor").msgclass
  FindAllBranchesRequest = Google::Protobuf::DescriptorPool.generated_pool.lookup("gitaly.FindAllBranchesRequest").msgclass
  FindAllBranchesResponse = Google::Protobuf::DescriptorPool.generated_pool.lookup("gitaly.FindAllBranchesResponse").msgclass
  FindAllBranchesResponse::Branch = Google::Protobuf::DescriptorPool.generated_pool.lookup("gitaly.FindAllBranchesResponse.Branch").msgclass
  FindAllTagsRequest = Google::Protobuf::DescriptorPool.generated_pool.lookup("gitaly.FindAllTagsRequest").msgclass
  FindAllTagsResponse = Google::Protobuf::DescriptorPool.generated_pool.lookup("gitaly.FindAllTagsResponse").msgclass
  FindAllTagsResponse::Tag = Google::Protobuf::DescriptorPool.generated_pool.lookup("gitaly.FindAllTagsResponse.Tag").msgclass
end
